module.exports = {
	hostRules: [
		// ARL = anti-rate-limiting
		{
			matchHost: 'docker.io',
			username: process.env.ARL_DOCKER_IO_USERNAME,
			password: process.env.ARL_DOCKER_IO_PASSWORD,
		},
		{
			matchHost: 'harbor.schober.space',
			username: process.env.ARL_HARBOR_SCHOBER_SPACE_USERNAME,
			password: process.env.ARL_HARBOR_SCHOBER_SPACE_PASSWORD,
		}
	],
	//allowedPostUpgradeCommands: ['.'],
};

